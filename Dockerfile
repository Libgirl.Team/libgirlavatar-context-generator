FROM pytorch/pytorch:latest

WORKDIR /usr/src/app

COPY ./requirements.txt /usr/src/app/requirements.txt
RUN pip install --upgrade pip && pip install -r requirements.txt

# Set python build options
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV FLASK_ENV production

EXPOSE 80

COPY . /usr/src/app

CMD ["gunicorn", "--bind", "0.0.0.0:80", "flask_app:app"]
