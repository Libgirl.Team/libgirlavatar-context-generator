#/bin/bash
string=$1
model=$2
echo $string
model_path='model/final_model'$2
para_size='124M'
if [[ "$para_size" =~ ^(82M|124M|355M|774M) ]]; then
        python3 ./generate.py --length=50 --nsamples=1 --prefix="${string}" --fast_pattern --save_samples --model_config=config/model_config_${para_size}.json --length=200 --model_path=$model_path --tokenizer_path='cache/vocab_bert-base-cased.txt'
else
        echo "$WORD is not in the list"
        echo "[82M|124M|355M|774M]"
fi
