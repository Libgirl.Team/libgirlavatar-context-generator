from reply_text import gen_text, load_model
from threading import Thread
import lambda_handler

device = "cpu"
model = load_model(device)
class ReplyProcessingJob(Thread):
    def __init__(self, payload):
        Thread.__init__(self)
        self.payload = payload

    def run(self):
        print("Handling request in background")
        prefix = self.payload["prefix"]
        user = self.payload["user"]

        text = gen_text(prefix, model, device)

        message = { "replyTask": {
            'messages': [{
                'text': text,
                'strategies': 'STRATEGY_ENG_TEST'
            }],
            'user': user
        }}
        lambda_handler.publish(message)


def _validate_user(user):
    return isinstance(user, dict) and user["platform"] in ("LINE", "FB") and user["id"] is not None

def _validate_payload(payload):
    return isinstance(payload, dict) and _validate_user(payload["user"]) and payload["prefix"]


def process_reply_in_background(payload):
    if _validate_payload(payload):
        thread = ReplyProcessingJob(payload)
        thread.start()
        return thread
    else:
        return False
