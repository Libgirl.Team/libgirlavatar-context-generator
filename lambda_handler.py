import json
import boto3
#  import unzip_requirements
import generate as gn
from transformers import GPT2LMHeadModel

def publish(payload):
    client = boto3.client('sns')
    # TODO: We shouldn't hardcode resource identifiers!
    response = client.publish(
        TargetArn='arn:aws:sns:us-east-1:436453500992:staging-us-reply-sender',
        Message=json.dumps(payload)
    )
    return response

def lambda_handler(event, context):
    print(event)
    src_msg = json.loads(event['Records'][0]['Sns']['Message'])
    print(src_msg)
    text = gen_text(src_msg['text'])
    message = { "replyTask": {
        'messages': [{
            'text': text,
            'strategies': 'STRATEGY_ENG_TEST'
        }],
        'user': src_msg['user']
    }}
    publish(message)
    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }
