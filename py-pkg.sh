if [ "$1" == "" ]
then
    opt=all
else
    opt=$1
fi

if [ $opt == "reply-generator" ] || [ $opt == "all" ]
then
    rm -r dist/*
    zip -r dist/py-reply-generator.zip lambda_handler.py generate.py tokenizations/ model/final_model/config.json
fi

if [ $opt == "all" ] || [ $opt == "pkgs" ]
then
    rm dist/python.zip
    cd docker_io/
    zip -r python.zip python -x \*boto3\* \*botocore\* \*dist-info\* \*click/\* \*docutils/\* \*tests\* \*__pycache__/\* \*.pyc \*.pyo \*torch\*
    mv python.zip ../dist/.
    cd ..
    # cd $PY_PKGS_PARENT
    # zip -r python.zip site-packages/* -x "*/__pycache__/*"
    # mv python.zip ${OLDPWD}/.
    # cd ${OLDPWD}
    #cd package/
    #zip -r dist/python.zip python/tqdm python/transformers/ python/sklearn/ python/regex/
    #mv python.zip ../dist/.
    #cd ..
fi


if [ $opt == "docker-gen-pkgs" ]
then
    echo "run docker"
    docker run --rm -v $(pwd)/docker_io:/io -t baliuzeger/quiltdata-lambda \
           bash /io/pkg.sh
           
fi

