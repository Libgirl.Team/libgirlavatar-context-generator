from flask import Flask, request
from process_reply import process_reply_in_background

app = Flask(__name__)

# Health check route for monitoring
@app.route("/healthcheck", methods=["GET"])
def ping():
    return "", 200

# POST /api/context
# Validates payload and begins message processing of a message in a thread,
# responds with 200 if the payload was valid or with 422 if it wasn't
@app.route('/api/context', methods=["POST"])
def get_reply_msg():
    if process_reply_in_background(request.json):
        return "", 200
    else:
        return "", 422

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
