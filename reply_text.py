import os
import boto3
from transformers import GPT2LMHeadModel
import generate as gn

def gen_text(prefix, model, device):
    gen_length = 200
    batch_size = 1
    n_samples = 1
    temperature = 1
    topk = 8
    topp = 0
    repetition_penalty = 1.0
    is_fast_pattern = True
    para_size = '124M'
    model_config = "config/model_config_{psz}".format(psz=para_size)
    model_path = 'model/final_model' # !!!!! need check
    tokenizer_path='cache/vocab_bert-base-cased.txt'
    if_segment = False

    if if_segment:
        from tokenizations import tokenization_bert_word_level as tokenization_bert
    else:
        from tokenizations import tokenization_bert

    os.environ["CUDA_VISIBLE_DEVICES"] = '0,1,2,3'  # 此处设置程序使用哪些显卡
    
    tokenizer = tokenization_bert.BertTokenizer(vocab_file=tokenizer_path)
    n_ctx = model.config.n_ctx

    if gen_length == -1:
        gen_length = model.config.n_ctx
        
    # if args.save_samples:
    #     if not os.path.exists(args.save_samples_path):
    #         os.makedirs(args.save_samples_path)
    #     samples_file = open(args.save_samples_path + '/samples.txt', 'w', encoding='utf8')

    generated = 0
    while generated < n_samples:
        raw_text = prefix
        context_tokens = tokenizer.convert_tokens_to_ids(tokenizer.tokenize(raw_text))
        for _ in range(n_samples // batch_size):
            out = gn.generate(
                n_ctx=n_ctx,
                model=model,
                context=context_tokens,
                length=gen_length,
                is_fast_pattern=is_fast_pattern, tokenizer=tokenizer,
                temperature=temperature, top_k=topk, top_p=topp, repitition_penalty=repetition_penalty, device=device
            )
            for i in range(batch_size):
                generated += 1
                text = tokenizer.convert_ids_to_tokens(out)
                for i, item in enumerate(text[:-1]):  # 确保英文前后有空格
                    if gn.is_word(item) and gn.is_word(text[i + 1]):
                        text[i] = item + ' '
                for i, item in enumerate(text):
                    if item == '[MASK]':
                        text[i] = ''
                    elif item == '[CLS]':
                        text[i] = '\n\n'
                    elif item == '[SEP]':
                        text[i] = '\n'
                info = "=" * 40 + " SAMPLE " + str(generated) + " " + "=" * 40 + "\n"
                print(info)
                text = ''.join(text).replace('##', '').strip()
                print(text)
        print("=" * 80)
    return text

def load_model(device):
    s3 = boto3.client('s3')
    model_dir = 'model/final_model'
    model_path = model_dir + '/pytorch_model.bin'
    S3_BUCKET = 'libgirlavatar-stage'
    OBJ_NAME = 'pytorch_model.bin'
    os.makedirs(model_dir, exist_ok=True)
    print(os.listdir('model'))
    print(os.listdir(model_dir))
    # s3.download_file(S3_BUCKET, OBJ_NAME, model_path)
    # md_obj = s3.get_object(Bucket=S3_BUCKET, Key=OBJ_NAME)
    model = GPT2LMHeadModel.from_pretrained(model_dir)
    model.to(device)
    model.eval()
    return model
