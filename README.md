## Context generator for the libgirlavatar project

### Project description

This repository contains the source code of the "context generator" module of the libgirlavatar
project. It is responsible for the machine learning part of the project, training, and response generation.
It has been removed from the original repository due to differences in technological stack
and deployment methodology.

While the majority of the libgirlavatar project is implemented as Node.js lambda functions, this project
is a Dockerized Python HTTP server. The 

### Deployment

In staging, the 

## Usage

1. download & unzip the [model dir](https://drive.google.com/file/d/1sXrXKOz-cT_mhA868ZB0EOxbhI2tLR-S/view?usp=sharing) into py-reply-renerator.
2. install python depencies: `pip install requirements.txt`
3. generate text by running `./generate_with.sh  "chara1:fooooo[SEP]"` (replace "foooo" by the input words).

[reference project](https://github.com/Morizeyao/GPT2-Chinese)
