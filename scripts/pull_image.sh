#!/bin/sh

ECR_REPO=${ECR_REPO:-436453500992.dkr.ecr.us-east-1.amazonaws.com/reply-gen:latest}

set -e

$(aws ecr get-login --no-include-email --region us-east-1)
docker pull $ECR_REPO
