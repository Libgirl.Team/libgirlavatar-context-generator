#!/bin/sh

SYSTEMD_UNIT=${SYSTEMD_UNIT:-context_generator.docker.service}

set -e

systemctl restart $SYSTEMD_UNIT
